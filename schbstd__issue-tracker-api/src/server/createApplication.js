import express from 'express';
import bodyParser from 'body-parser';
import Store from './store/Store';
import storeMiddleware from './store/storeMiddleware';
import createRouter from './createRouter';
import api from './routes/api';
import { APP_NAME, APP_PORT } from './constants';

const createServer = (
  middleware = () => [],
  { provisionedData, defaultExpress = express } = {},
) => {
  const app = defaultExpress();
  const store = new Store(provisionedData);

  app.use([
    bodyParser.urlencoded({ extended: false }),
    bodyParser.json(),
    storeMiddleware(store),
    middleware(app),
  ]);
  app.use('/api', createRouter(api));

  app.get('/', (req, res) => {
    res.send({});
  });

  app.listen = (_super => (...args) => {
    _super.apply(
      app,
      args.length
        ? args
        : [
            APP_PORT,
            () => {
              // eslint-disable-next-line no-console
              console.log(`${APP_NAME} listening on port ${APP_PORT}...`);
            },
          ],
    );
  })(app.listen);

  return app;
};

export default createServer;
