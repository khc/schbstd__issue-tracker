import morgan from 'morgan';
import cors from 'cors';
import createApplication from './createApplication';

const provisionedData = [
  {
    _id: '1',
    title: 'Some title',
    description: 'desc...',
    status: 'OPEN',
  },
];

createApplication(() => [morgan('dev'), cors()], { provisionedData }).listen();
