import Router from 'express';

const createRouter = (routes, router = new Router()) => {
  routes(router);
  return router;
};

export default createRouter;
