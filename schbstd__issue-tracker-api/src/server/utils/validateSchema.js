import Ajv from 'ajv';
import issueSchema from '../schema/issue.schema.json';

const ajv = new Ajv();
ajv.addSchema(issueSchema, 'issue');

const validateSchema = (schema, payload) =>
  new Promise((resolve, reject) =>
    ajv.validate(schema, payload) ? resolve(payload) : reject(new Error(ajv.errors[0].message)),
  );

export default validateSchema;
