export const propsFilter = (props, response) => {
  return props.split(',').reduce((acc, prop) => ({ ...acc, [prop]: response[prop] }), {});
};

const responseFilter = (query, response) =>
  query.props
    ? response.map(({ _id, ...rest }) => ({ _id, ...propsFilter(query.props, rest) }))
    : response;

export default responseFilter;
