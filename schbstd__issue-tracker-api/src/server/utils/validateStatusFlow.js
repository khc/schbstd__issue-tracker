import { MESSAGES } from '../constants';

const validateStatusFlow = (flow, currentStatus, nextStatus) =>
  flow.findIndex(status => status === nextStatus) >=
  flow.findIndex(status => status === currentStatus)
    ? Promise.resolve(nextStatus)
    : Promise.reject(new Error(MESSAGES.STATUS_FLOW_ERROR));

export default validateStatusFlow;
