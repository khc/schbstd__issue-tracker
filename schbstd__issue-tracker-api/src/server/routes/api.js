import validateSchema from '../utils/validateSchema';
import validateStatusFlow from '../utils/validateStatusFlow';
import responseFilter from '../utils/responseFilter';
import { ISSUE_STATUS_FLOW } from '../constants';

const api = router =>
  router
    .get('/issues/:id', ({ params }, res) => {
      res.locals.store
        .select({ _id: params.id })
        .then(issue => res.send(issue))
        .catch(err => res.status(404).send(err.message));
    })
    .get('/issues', (req, res) => {
      res.locals.store
        .selectAll()
        .then(issues => responseFilter(req.query, issues))
        .then(issues => res.send(issues));
    })
    .post('/issues', ({ body }, res) => {
      validateSchema('issue', body)
        .then(() => res.locals.store.insert({ body }))
        .then(issue => res.send(issue))
        .catch(err => res.status(400).send(err.message));
    })
    .put('/issues/:id', ({ body, params }, res) => {
      validateSchema('issue', body)
        .then(() => res.locals.store.select({ _id: params.id }))
        .then(({ status }) => validateStatusFlow(ISSUE_STATUS_FLOW, status, body.status))
        .then(() => res.locals.store.update({ _id: params.id, body }))
        .then(issue => res.send(issue))
        .catch(err => res.status(400).send(err.message));
    });

export default api;
