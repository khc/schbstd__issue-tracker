const storeMiddleware = store => (req, res, next) => {
  res.locals.store = res.locals.store || store;
  next();
};

export default storeMiddleware;
