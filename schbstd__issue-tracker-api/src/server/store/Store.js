import { map } from 'ramda';
import { resolve, reject, findById, createRecord, nextRecord } from './storeUtils';
import { MESSAGES } from '../constants';

class Store {
  constructor(store = []) {
    this.store = store;
  }

  selectAll = () => resolve(this.store);

  select = ({ _id }) => {
    const existingRecord = findById(_id)(this.store);
    return existingRecord ? resolve(existingRecord) : reject(MESSAGES.ISSUE_NOT_FOUND)();
  };

  update = ({ _id, body }) => {
    if (!findById(_id)(this.store)) {
      return reject(MESSAGES.ISSUE_NOT_FOUND)();
    }

    const newRecord = createRecord(body)(_id);
    // eslint-disable-next-line no-underscore-dangle
    this.store = map(record => (record._id === _id ? newRecord : record))(this.store);

    return resolve(newRecord);
  };

  insert = ({ body }) => {
    const newRecord = nextRecord(body)(this.store);

    this.store = [...this.store, newRecord];

    return resolve(newRecord);
  };
}

export default Store;
