/* eslint-disable import/prefer-default-export */
import { find, propEq, reduce, max, pipe } from 'ramda';

export const findById = _id => find(propEq('_id', _id));

export const nextId = reduce((acc, { _id }) => max(acc, parseInt(_id, 10) + 1), 1);

export const createRecord = body => _id => ({
  _id: _id.toString(),
  ...body,
});

export const nextRecord = body => pipe(nextId, createRecord(body));

export const resolve = result => Promise.resolve(result);

export const reject = message => () => Promise.reject(new Error(message));
