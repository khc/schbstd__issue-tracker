/* eslint-disable import/prefer-default-export */
export const EVENTS = {
  SELECT_ALL: 'SELECT_ALL',
  SELECT: 'SELECT',
  INSERT: 'INSERT',
  UPDATE: 'UPDATE',
  ERROR: 'ERROR',
};

export const ISSUE_STATUS = {
  OPEN: 'OPEN',
  PENDING: 'PENDING',
  CLOSED: 'CLOSED',
};

export const ISSUE_STATUS_FLOW = [ISSUE_STATUS.OPEN, ISSUE_STATUS.PENDING, ISSUE_STATUS.CLOSED];

export const MESSAGES = {
  ISSUE_NOT_FOUND: 'Issue has not been found',
  STATUS_FLOW_ERROR: 'Status cannot be reversed to previous value',
};

export const { APP_NAME = 'api', APP_PORT = 3000 } = process.env;
