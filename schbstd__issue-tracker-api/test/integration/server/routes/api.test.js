import request from 'supertest';
import createApplication from '../../../../src/server/createApplication';
import responseFilter from '../../../../src/server/utils/responseFilter';
import { MESSAGES, ISSUE_STATUS } from '../../../../src/server/constants';

const provisionedData = [
  {
    _id: '1',
    title: 'title1',
    description: 'description1',
    status: ISSUE_STATUS.OPEN,
  },
  {
    _id: '2',
    title: 'title2',
    description: 'description2',
    status: ISSUE_STATUS.CLOSED,
  },
];

describe('src/server/routes/api', () => {
  let app;
  let response;

  beforeEach(() => {
    app = createApplication(() => [], { provisionedData });
  });

  afterEach(() => {
    response = undefined;
  });

  describe('GET', () => {
    it('should get all data', () => {
      response = request(app).get('/api/issues');

      expect(response).resolves.toMatchObject({
        status: 200,
        body: provisionedData,
      });
    });
    it('should filter props by query string', () => {
      response = request(app).get('/api/issues?props=title');

      expect(response).resolves.toMatchObject({
        status: 200,
        body: responseFilter({ props: 'title' }, provisionedData),
      });
    });
    it('should return record by id', () => {
      response = request(app).get('/api/issues/1');

      expect(response).resolves.toMatchObject({
        status: 200,
        body: provisionedData[0],
      });
    });
    it('should return error when id is not found', () => {
      response = request(app).get('/api/issues/3');
      expect(response).resolves.toMatchObject({
        status: 404,
        text: MESSAGES.ISSUE_NOT_FOUND,
      });
    });
  });
  describe('POST', () => {
    it('should insert new record', async () => {
      const body = {
        title: 'title3',
        description: 'description3',
        status: ISSUE_STATUS.PENDING,
      };

      response = await request(app)
        .post('/api/issues')
        .send(body);

      expect(response).toMatchObject({
        status: 200,
        body: { _id: '3', ...body },
      });

      response = await request(app).get('/api/issues');

      expect(response).toMatchObject({
        status: 200,
        body: [...provisionedData, { _id: '3', ...body }],
      });
    });
    it('should validate body', async () => {
      const body = {
        title: 'title3',
      };

      response = await request(app)
        .post('/api/issues')
        .send(body);

      expect(response).toMatchObject({
        status: 400,
        text: "should have required property 'description'",
      });
    });
  });
  describe('PUT', () => {
    it('should update data', async () => {
      const body = {
        title: 'updated title',
        description: 'updated description',
        status: ISSUE_STATUS.OPEN,
      };

      response = await request(app)
        .put('/api/issues/1')
        .send(body);

      expect(response).toMatchObject({
        status: 200,
        body: {
          _id: '1',
          ...body,
        },
      });
    });
    it('should response with an error when id is not found', async () => {
      const body = {
        title: 'updated title',
        description: 'updated description',
        status: ISSUE_STATUS.OPEN,
      };

      response = await request(app)
        .put('/api/issues/3')
        .send(body);

      expect(response).toMatchObject({
        status: 400,
        text: MESSAGES.ISSUE_NOT_FOUND,
      });
    });
    it('should valiate body', async () => {
      const body = {
        title: 'updated title',
      };

      response = await request(app)
        .put('/api/issues/3')
        .send(body);

      expect(response).toMatchObject({
        status: 400,
        text: "should have required property 'description'",
      });
    });
  });
});
