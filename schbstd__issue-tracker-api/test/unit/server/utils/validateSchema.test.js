import validateSchema from '../../../../src/server/utils/validateSchema';
import { ISSUE_STATUS } from '../../../../src/server/constants';

describe('src/server/utils/validateSchema', () => {
  describe('validateSchema', () => {
    const payload = {
      title: 'Irure labore ut ad cupidatat.',
      description:
        'Aliquip pariatur anim qui est sunt sit aliquip nisi laboris qui Lorem ipsum ex.',
      status: ISSUE_STATUS.OPEN,
    };

    it('should resolve when payload is valid', () => {
      const result = validateSchema('issue', payload);
      expect(result).resolves.toEqual(payload);
    });
    it('should reject when payload is invalid', () => {
      const result = validateSchema('issue', {});
      expect(result).rejects.toEqual(new Error("should have required property 'title'"));
    });
  });
});
