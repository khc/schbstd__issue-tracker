import validateStatusFlow from '../../../../src/server/utils/validateStatusFlow';
import { ISSUE_STATUS_FLOW, ISSUE_STATUS, MESSAGES } from '../../../../src/server/constants';

describe('src/server/utils/validateStatusFlow', () => {
  describe('validateStatusFlow', () => {
    it('should allow for move status forward', () => {
      const result = validateStatusFlow(ISSUE_STATUS_FLOW, ISSUE_STATUS.OPEN, ISSUE_STATUS.PENDING);
      expect(result).resolves.toEqual(ISSUE_STATUS.PENDING);
    });
    it('should disallow to move status backward', () => {
      const result = validateStatusFlow(ISSUE_STATUS_FLOW, ISSUE_STATUS.PENDING, ISSUE_STATUS.OPEN);
      expect(result).rejects.toEqual(new Error(MESSAGES.STATUS_FLOW_ERROR));
    });
  });
});
