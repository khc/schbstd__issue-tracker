import responseFilter, { propsFilter } from '../../../../src/server/utils/responseFilter';

describe('src/server/utils/responseFilter', () => {
  const response = {
    prop1: 'Esse eiusmod do nisi esse.',
    prop2: 'Minim excepteur sint fugiat duis excepteur.',
    prop3: 'Labore aliqua ex aute sunt officia commodo dolore cillum.',
  };

  describe('propsFilter', () => {
    it('should filter props from response', () => {
      const filteredResponse = propsFilter('prop1,prop3', response);
      expect(filteredResponse).toEqual({
        prop1: response.prop1,
        prop3: response.prop3,
      });
    });
  });
  describe('responseFilter', () => {
    it('shoud filter props from response when props query is provided', () => {
      const filteredResponse = responseFilter({ props: 'prop2' }, [{ _id: '1', ...response }]);
      expect(filteredResponse).toEqual([
        {
          _id: '1',
          prop2: response.prop2,
        },
      ]);
    });
    it('should not filter props from response when props query is not provided', () => {
      const filteredResponse = responseFilter({}, [{ _id: '1', ...response }]);
      expect(filteredResponse).toEqual([{ _id: '1', ...response }]);
    });
  });
});
