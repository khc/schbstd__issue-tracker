/* eslint-disable func-names */
import api from '../../../../src/server/routes/api';

describe('src/server/routes/api', () => {
  describe('api', () => {
    const chain = function() {
      return this;
    };
    const mockRouter = {
      get: jest.fn().mockImplementation(chain),
      post: jest.fn().mockImplementation(chain),
      put: jest.fn().mockImplementation(chain),
    };

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should mount routes', () => {
      const router = api(mockRouter);

      expect(router.get).toBeCalledTimes(2);
      expect(router.get).toBeCalledWith('/issues/:id', expect.any(Function));
      expect(router.get).toBeCalledWith('/issues', expect.any(Function));
      expect(router.post).toBeCalledTimes(1);
      expect(router.post).toBeCalledWith('/issues', expect.any(Function));
      expect(router.put).toBeCalledTimes(1);
      expect(router.put).toBeCalledWith('/issues/:id', expect.any(Function));
    });
  });
});
