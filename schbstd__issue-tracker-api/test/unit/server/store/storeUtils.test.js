import {
  resolve,
  reject,
  findById,
  nextId,
  createRecord,
  nextRecord,
} from '../../../../src/server/store/storeUtils';

describe('src/server/store/storeUtils', () => {
  const payload = {
    title: 'Nostrud nostrud ea reprehenderit minim duis aliqua.',
    description: 'Ullamco ex mollit mollit eu culpa sint tempor.',
    status: 'OPEN',
  };
  const store = [
    {
      _id: '1',
      ...payload,
    },
    {
      _id: '2',
      ...payload,
    },
  ];

  describe('resolve', () => {
    it('should resolve a value', () => {
      expect(resolve(true)).resolves.toEqual(true);
    });
  });
  describe('reject', () => {
    it('should rejects with an error', () => {
      expect(reject('message')()).rejects.toEqual(new Error('message'));
    });
  });
  describe('findById', () => {
    it('should return found record when called with store', () => {
      expect(findById('1')).toBeInstanceOf(Function);
      expect(findById('1')(store)).toEqual(store[0]);
    });
  });
  describe('nextId', () => {
    it('should return 1 when store is empty', () => {
      expect(nextId([])).toEqual(1);
    });
    it('should return next id', () => {
      expect(nextId(store)).toEqual(store.length + 1);
    });
  });
  describe('createRecord', () => {
    it('should create record body', () => {
      expect(createRecord(payload)).toBeInstanceOf(Function);
      expect(createRecord(payload)(1)).toEqual(store[0]);
    });
  });
  describe('nextRecord', () => {
    it('should return record with next id', () => {
      expect(nextRecord(payload)(store)).toEqual({ _id: '3', ...payload });
    });
  });
});
