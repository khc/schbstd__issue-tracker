import storeMiddleware from '../../../../src/server/store/storeMiddleware';

describe('src/server/store/storeMiddleware', () => {
  describe('storeMiddleware', () => {
    let req;
    let res;
    let next;

    beforeEach(() => {
      next = jest.fn();
      res = {
        locals: {},
      };
    });
    afterEach(() => {
      next.mockRestore();
    });
    it('should add store to res.locals', () => {
      storeMiddleware('new store')(req, res, next);
      expect(res.locals).toEqual({
        store: 'new store',
      });
      expect(next).toBeCalled();
    });
    it('shound not add store if one exists', () => {
      res.locals.store = 'existing store';
      storeMiddleware('new store')(req, res, next);
      expect(res.locals).toEqual({
        store: 'existing store',
      });
      expect(next).toBeCalled();
    });
  });
});
