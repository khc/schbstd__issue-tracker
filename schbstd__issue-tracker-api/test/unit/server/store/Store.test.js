import Store from '../../../../src/server/store/Store';
import { MESSAGES } from '../../../../src/server/constants';

const provisionedData = [
  {
    _id: '1',
    prop: 'some data',
  },
  {
    _id: '2',
    prop: 'more data',
  },
];

describe('src/server/store/Store', () => {
  describe('Store', () => {
    let store;

    beforeEach(() => {
      store = new Store(provisionedData);
    });
    afterEach(() => {
      store = null;
    });
    it('should create empty Store and resolve data', () => {
      const emptyStore = new Store();
      expect(emptyStore).toBeInstanceOf(Store);
      expect(emptyStore.selectAll()).resolves.toEqual([]);
    });
    it('should create store with provisioned data', () => {
      expect(store).toBeInstanceOf(Store);
      expect(store.selectAll()).resolves.toEqual(provisionedData);
    });
    it('should resolve record when id is found', () => {
      expect(store.select({ _id: '1' })).resolves.toEqual(provisionedData[0]);
    });
    it('should reject with an error when id is not found', () => {
      expect(store.select({ _id: '3' })).rejects.toEqual(new Error(MESSAGES.ISSUE_NOT_FOUND));
    });
    it('should take new record, resolve and append to store', () => {
      const nextBody = {
        prop: 'inserted data',
      };
      const expectedRecord = {
        _id: (provisionedData.length + 1).toString(),
        ...nextBody,
      };
      expect(store.insert({ body: nextBody })).resolves.toEqual(expectedRecord);
      expect(store.selectAll()).resolves.toEqual([...provisionedData, expectedRecord]);
    });
    it('should take updated record, resolve and update store', () => {
      const updatedBody = {
        prop: 'updated data',
      };
      const expectedRecord = {
        ...provisionedData[0],
        prop: 'updated data',
      };
      expect(store.update({ _id: '1', body: updatedBody })).resolves.toEqual(expectedRecord);
    });
    it('should reject with an error when id is not found', () => {
      expect(store.update({ _id: '3', body: {} })).rejects.toEqual(
        new Error(MESSAGES.ISSUE_NOT_FOUND),
      );
    });
  });
});
