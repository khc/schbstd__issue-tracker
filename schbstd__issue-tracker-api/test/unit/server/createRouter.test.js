import createRouter from '../../../src/server/createRouter';

describe('src/server/createRouter', () => {
  describe('createRouter', () => {
    let routes;

    beforeEach(() => {
      routes = jest.fn();
    });
    afterEach(() => {
      routes.mockRestore();
    });
    it('should ', () => {
      createRouter(routes, 'router');
      expect(routes).toBeCalledWith('router');
    });
  });
});
