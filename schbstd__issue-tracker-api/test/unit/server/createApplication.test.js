import createApplication from '../../../src/server/createApplication';

describe('src/server/createApplication', () => {
  describe('createApplication', () => {
    let app;
    const defaultExpress = () => ({
      use: jest.fn(),
      get: jest.fn(),
      listen: jest.fn(),
    });

    beforeEach(() => {
      app = createApplication(() => [], { defaultExpress });
    });

    afterEach(() => {
      app = undefined;
      jest.restoreAllMocks();
    });

    it('should create express application', () => {
      expect(app.listen).toBeInstanceOf(Function);
      expect(app.use).toBeCalledWith(expect.any(Array));
      expect(app.use).toBeCalledWith('/api', expect.any(Function));
      expect(app.use).toBeCalledWith('/api', expect.any(Function));
      expect(app.get).toBeCalledWith('/', expect.any(Function));
    });
  });
});
