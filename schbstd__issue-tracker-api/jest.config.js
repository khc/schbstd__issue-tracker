module.exports = {
  testMatch: ['**/test/**/*.test.{js,jsx}'],
  setupFilesAfterEnv: ['./test/setup.js'],
  coverageReporters: ['text', 'lcov'],
  collectCoverageFrom: [
    '**/src/**/*.{js,jsx}',
    '!**/src/server/constants.js',
    '!**/src/server/index.js',
    '!**/src/server/index-dev.js',
  ],
};
