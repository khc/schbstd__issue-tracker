module.exports = {
  extends: ['airbnb-base', 'prettier'],
  plugins: ['prettier'],
  parser: 'babel-eslint',
  rules: {
    'prettier/prettier': 2,
    'import/no-extraneous-dependencies': [
      2, {
        'devDependencies': true,
      },
    ],
  },
  env: {
    browser: true,
    node: true,
  },
};