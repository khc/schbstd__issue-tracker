
DOCKER := docker
DOCKER-COMPOSE := docker-compose
RUN := ${DOCKER-COMPOSE} run --rm app
EXEC := ${DOCKER-COMPOSE} exec app

# Development

dev_install:
	${RUN} npm install

dev_run_console:
	${RUN} zsh

dev_exec_console:
	${EXEC} zsh

dev_start:
	${DOCKER-COMPOSE} up