module.exports = {
  testMatch: ['**/test/**/*.test.{js,jsx}'],
  setupFilesAfterEnv: ['./test/setup.js'],
  coverageReporters: ['text', 'lcov'],
  collectCoverageFrom: [
    '**/src/client/**/*.{js,jsx}',
    '!**/src/client/index.jsx',
    '!**/src/client/Application.jsx',
  ],
};
