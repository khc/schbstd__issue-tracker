import { hot } from 'react-hot-loader';
import React from 'react';
import { render } from 'react-dom';
import Application from './Application';

const HotApplication = hot(module)(Application);

render(<HotApplication />, document.getElementById('application'));

module.hot.accept();
