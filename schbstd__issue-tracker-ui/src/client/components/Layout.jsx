import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import { GridRow, Container, Header } from 'semantic-ui-react';
import { APP_TITLE, APP_DESCRIPTION } from '../constants';

import IssueCreate from '../pages/IssueCreate';
import IssueDetails from '../pages/IssueDetails';
import IssueList from '../pages/IssueList';

const Layout = () => (
  <Fragment>
    <GridRow>
      <Container>
        <Header as="h1">
          {APP_TITLE}
          <Header.Subheader>{APP_DESCRIPTION}</Header.Subheader>
        </Header>
        <Switch>
          <Route path="/issue/create">
            <IssueCreate />
          </Route>
          <Route path="/issue/:id">
            <IssueDetails />
          </Route>
          <Route path="/">
            <IssueList />
          </Route>
        </Switch>
      </Container>
    </GridRow>
  </Fragment>
);

export default Layout;
