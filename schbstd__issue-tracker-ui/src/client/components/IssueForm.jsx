import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Message, Button } from 'semantic-ui-react';
import { ISSUE_STATUS } from '../constants';

const options = [
  { key: ISSUE_STATUS.OPEN, text: ISSUE_STATUS.OPEN, value: ISSUE_STATUS.OPEN },
  { key: ISSUE_STATUS.PENDING, text: ISSUE_STATUS.PENDING, value: ISSUE_STATUS.PENDING },
  { key: ISSUE_STATUS.CLOSED, text: ISSUE_STATUS.CLOSED, value: ISSUE_STATUS.CLOSED },
];

class IssueForm extends Component {
  constructor(props) {
    super(props);
    const { title, description, status } = props;
    this.state = {
      title,
      description,
      status: status || ISSUE_STATUS.OPEN,
    };
  }

  changeHandler = fieldName => (e, data) => {
    this.setState({
      [fieldName]: data.value,
    });
  };

  render = () => {
    const { title, description, status } = this.state;
    const { saveHandler, cancelHandler, message } = this.props;
    return (
      <Form>
        {message && <Message color="red">{message}</Message>}
        <Form.Input label="Title" defaultValue={title} onChange={this.changeHandler('title')} />
        <Form.TextArea
          label="Description"
          defaultValue={description}
          onChange={this.changeHandler('description')}
        />
        <Form.Select
          label="Status"
          options={options}
          disabled={!status}
          defaultValue={status}
          onChange={this.changeHandler('status')}
        />
        <Button primary onClick={saveHandler(this.state)}>
          Save
        </Button>
        <Button onClick={cancelHandler()}>Cancel</Button>
      </Form>
    );
  };
}

IssueForm.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  status: PropTypes.string,
  message: PropTypes.string,
  saveHandler: PropTypes.func.isRequired,
  cancelHandler: PropTypes.func.isRequired,
};

IssueForm.defaultProps = {
  title: null,
  description: null,
  status: ISSUE_STATUS.OPEN,
  message: null,
};

export default IssueForm;
