/* eslint-disable import/prefer-default-export */
import { createAction } from 'redux-actions';

export const FILL_ISSUES = 'SET_ISSUES';
export const fillIssues = createAction(FILL_ISSUES, issues => issues);

export const ADD_ISSUE = 'ADD_ISSUE';
export const addIssue = createAction(ADD_ISSUE, issue => issue);

export const UPDATE_ISSUE = 'UPDATE_ISSUE';
export const updateIssue = createAction(UPDATE_ISSUE, issue => issue);
