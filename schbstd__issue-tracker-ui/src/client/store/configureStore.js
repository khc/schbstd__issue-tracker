import { createStore, combineReducers, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';

import issuesReducer from './reducers/issuesReducer';

const configureStore = (initialState = {}, enableLogger = false) => {
  const middleware = [
    promiseMiddleware,
    thunkMiddleware,
    ...(enableLogger ? [loggerMiddleware] : []),
  ];

  const store = createStore(
    combineReducers({
      issues: issuesReducer,
    }),
    initialState,
    applyMiddleware(...middleware),
  );

  return { store };
};

export default configureStore;
