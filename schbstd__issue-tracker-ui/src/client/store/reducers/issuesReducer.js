import { handleActions } from 'redux-actions';
import { FILL_ISSUES, ADD_ISSUE, UPDATE_ISSUE } from '../actions/issuesActions';

const issuesReducer = handleActions(
  {
    [FILL_ISSUES]: {
      next: (state, action) => action.payload,
    },
    [ADD_ISSUE]: {
      next: (state, action) => [...state, action.payload],
    },
    [UPDATE_ISSUE]: {
      next: (state, action) =>
        state.map(issue => (issue._id === action.payload._id ? action.payload : issue)),
    },
  },
  [],
);

export default issuesReducer;
