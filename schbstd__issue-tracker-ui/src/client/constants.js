/* eslint-disable import/prefer-default-export */
export const ISSUE_STATUS = {
  OPEN: 'OPEN',
  PENDING: 'PENDING',
  CLOSED: 'CLOSED',
};

export const ISSUE_COLOR = {
  [ISSUE_STATUS.OPEN]: 'red',
  [ISSUE_STATUS.PENDING]: 'green',
  [ISSUE_STATUS.CLOSED]: 'grey',
};

export const { APP_TITLE = 'Issues', APP_DESCRIPTION = 'Description', API_BASEPATH } = window.env;
