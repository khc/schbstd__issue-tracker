import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import axios from 'axios';
import Layout from './components/Layout';
import configureStore from './store/configureStore';
import { fillIssues } from './store/actions/issuesActions';
import { API_BASEPATH } from './constants';
import './styles/styles.scss';

const { store } = configureStore({}, { enableLogger: true });

axios.get(`${API_BASEPATH}/api/issues`).then(({ data }) => store.dispatch(fillIssues(data)));

const Application = () => (
  <ReduxProvider store={store}>
    <BrowserRouter>
      <Grid padded>
        <Layout />
      </Grid>
    </BrowserRouter>
  </ReduxProvider>
);

export default Application;
