import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Header, List, Label, Button } from 'semantic-ui-react';
import { ISSUE_COLOR } from '../constants';

const IssueList = ({ issues }) => (
  <>
    <Header as="h2">All issues</Header>
    <Button>
      <Link to="/issue/create">New issue</Link>
    </Button>
    <List divided relaxed verticalAlign="middle">
      {issues.map(issue => (
        <List.Item key={issue._id}>
          <List.Content floated="right">
            <Label color={ISSUE_COLOR[issue.status]}>{issue.status}</Label>
          </List.Content>
          <List.Icon name="bug" size="large" verticalAlign="middle" />
          <List.Content>
            <List.Header>
              <Link to={`/issue/${issue._id}`}>{issue.title}</Link>
            </List.Header>
            {issue.description}
          </List.Content>
        </List.Item>
      ))}
    </List>
  </>
);

IssueList.propTypes = {
  issues: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

const mapStateToProps = ({ issues }) => ({ issues });

export default connect(mapStateToProps)(IssueList);
