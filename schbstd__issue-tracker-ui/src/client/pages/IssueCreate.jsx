import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Header } from 'semantic-ui-react';
import axios from 'axios';
import { compose } from 'ramda';
import IssueForm from '../components/IssueForm';
import { addIssue } from '../store/actions/issuesActions';
import { API_BASEPATH } from '../constants';

class IssueCreate extends Component {
  state = {};

  saveHandler = state => () => {
    const { dispatch, history } = this.props;
    axios
      .post(`${API_BASEPATH}/api/issues`, state)
      .then(({ data }) => {
        dispatch(addIssue(data));
        history.replace('/');
      })
      .catch(err => {
        this.setState({
          message: err.response.data,
        });
      });
  };

  cancelHandler = () => () => {
    const { history } = this.props;
    history.goBack();
  };

  render = () => {
    const { message } = this.state;
    return (
      <>
        <Header as="h2">New issue</Header>
        <IssueForm
          saveHandler={this.saveHandler}
          cancelHandler={this.cancelHandler}
          message={message}
        />
      </>
    );
  };
}

IssueCreate.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

export default compose(withRouter, connect())(IssueCreate);
