import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'ramda';
import axios from 'axios';
import { Header } from 'semantic-ui-react';
import IssueForm from '../components/IssueForm';
import { updateIssue } from '../store/actions/issuesActions';
import { API_BASEPATH } from '../constants';

class IssueDetails extends Component {
  state = {};

  saveHandler = state => () => {
    const { match, dispatch, history } = this.props;
    axios
      .put(`${API_BASEPATH}/api/issues/${match.params.id}`, state)
      .then(({ data }) => {
        dispatch(updateIssue(data));
        history.push('/');
      })
      .catch(err => {
        this.setState({
          message: err.response.data,
        });
      });
  };

  cancelHandler = () => () => {
    const { history } = this.props;
    history.goBack();
  };

  render = () => {
    const {
      issue: { title, description, status },
    } = this.props;
    const { message } = this.state;
    return (
      <>
        <Header as="h2">{title}</Header>
        <IssueForm
          title={title}
          description={description}
          status={status}
          message={message}
          saveHandler={this.saveHandler}
          cancelHandler={this.cancelHandler}
        />
      </>
    );
  };
}

IssueDetails.propTypes = {
  issue: PropTypes.shape({
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
  }).isRequired,
  match: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const selectIssue = WrappedComponent => props => {
  const issue = props.issues.find(({ _id }) => _id === props.match.params.id);
  return issue ? <WrappedComponent {...props} issue={issue} /> : null;
};

const mapStateToProps = ({ issues }) => ({ issues });

export default compose(connect(mapStateToProps), withRouter, selectIssue)(IssueDetails);
