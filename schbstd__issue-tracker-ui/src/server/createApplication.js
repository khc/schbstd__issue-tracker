import express from 'express';
import { resolve } from 'path';

import { APP_NAME, APP_PORT, API_BASEPATH } from './constants';

const createServer = (middleware = () => []) => {
  const app = express();

  app.set('view engine', 'pug');
  app.set('views', resolve(__dirname, './views'));

  app.use('/public', express.static(resolve(__dirname, '../../public')));
  app.use(middleware(app));

  app.get('*', (req, res) => {
    res.render('index', {
      CONFIG: JSON.stringify({ API_BASEPATH }),
    });
  });

  app.listen = (_super => (...args) => {
    _super.apply(
      app,
      args.length
        ? args
        : [
            APP_PORT,
            () => {
              // eslint-disable-next-line no-console
              console.log(`${APP_NAME} listening on port ${APP_PORT}...`);
            },
          ],
    );
  })(app.listen);

  return app;
};

export default createServer;
