import morgan from 'morgan';
import { resolve } from 'path';
import Bundler from 'parcel-bundler';
import createApplication from './createApplication';

const bundler = new Bundler(resolve(__dirname, '../client/index-dev.jsx'), {
  outDir: resolve(__dirname, '../../public'),
  hmr: true,
});

createApplication(() => [morgan('dev'), bundler.middleware()]).listen();
