/* eslint-disable import/prefer-default-export */

export const {
  APP_NAME = 'schbstd__ui',
  APP_PORT = 3000,
  API_BASEPATH,
} = process.env;
