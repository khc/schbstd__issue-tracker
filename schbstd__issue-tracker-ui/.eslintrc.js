

module.exports = {
  extends: ['airbnb', 'prettier'],
  plugins: ['prettier'],
  parser: 'babel-eslint',
  rules: {
    'prettier/prettier': 2,
    'import/no-extraneous-dependencies': [
      2, {
        'devDependencies': true,
      },
    ],
    'no-underscore-dangle': 0,
  },
  env: {
    browser: true,
  },
};